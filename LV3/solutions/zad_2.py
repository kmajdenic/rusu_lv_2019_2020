# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#barplot
mtcars = pd.read_csv('../resources/mtcars.csv') 
cil4= mtcars[mtcars.cyl == 4]  
cil6= mtcars[mtcars.cyl == 6]
cil8= mtcars[mtcars.cyl == 8]

array_cil4=np.arange(0,len(cil4),1)
array_cil6=np.arange(0,len(cil6),1)
array_cil8=np.arange(0,len(cil8),1)

plt.figure()

plt.bar(array_cil4, cil4['mpg'],0.4, color='red')
plt.bar(array_cil6, cil6['mpg'],0.6, color='blue')
plt.bar(array_cil8, cil8['mpg'],0.8, color='green')

plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
plt.xlabel("Automobil")
plt.ylabel("Potrošnja u mpg")
plt.legend(["4 cilindra","6 cilindara","8 cilindara"])

#boxplot- distribucija mase 
cil4_masa=[]
for i in cil4["wt"]:
    cil4_masa.append(i)
    
cil6_masa=[]
for i in cil6["wt"]:
    cil6_masa.append(i)
    
cil8_masa=[]
for i in cil8["wt"]:
    cil8_masa.append(i)
    
plt.figure()
plt.boxplot([cil4_masa,cil6_masa,cil8_masa],positions=[4,6,8])
plt.title("Masa automobila s 4, 6 i 8 cilindara")
plt.xlabel("Broj cilindara")
plt.ylabel("Masa u lbs/1000")

#mpg pri automatskom i rucnom mjenjacu
automatic=mtcars[(mtcars.am== 1)]
mpg_automatic=[]
for i in automatic["mpg"]:
    mpg_automatic.append(i)
    
manual=mtcars[(mtcars.am== 0)]
mpg_manual=[]
for i in manual["mpg"]:
    mpg_manual.append(i)
    
plt.figure()
plt.boxplot([mpg_manual, mpg_automatic], positions = [0,1])
plt.title("Potrosnja automobila s rucnim i automatskim mjenjacem")
plt.ylabel('Potrosnja u mpg')
plt.xlabel('Rucni mjenjac, Automatski mjenjac')

#odnos ubrzanja i snage
acc_automatic=[]
for i in automatic["qsec"]:  
    acc_automatic.append(i)

hp_automatic=[]
for i in automatic["hp"]:  
    hp_automatic.append(i)

acc_manual=[]
for i in manual["qsec"]:  
    acc_manual.append(i)

hp_manual=[]
for i in manual["hp"]:  
    hp_manual.append(i)
    

plt.figure()
plt.scatter(acc_automatic, hp_automatic, marker='o')   
plt.scatter(acc_manual, hp_manual, marker='x')  
plt.title("Odnos ubrzanja i snage automobila s rucnim i automatskim mjenjacem")
plt.ylabel('Snaga')
plt.xlabel('Ubrzanje')
plt.legend(["Automatski mjenjac","Rucni mjenjac"])




