# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 15:52:44 2021

@author: Korisnik
"""
import pandas as pd

mtcars=pd.read_csv('../resources/mtcars.csv')

# 5 auta s najvecom potrosnjom
potrosnja_list=mtcars.sort_values(by=['mpg'])
print ("Najveci potrosaci: ")
print (potrosnja_list['car'].head(5))


# 3 auta s najmanjom potrosnjom od 8 cilindara
potrosnja_cil8=mtcars[mtcars.cyl==8]
sort_cil8=potrosnja_cil8.sort_values(by=['mpg'])
print ("Najmanji potrosaci sa 8 cilindara: ")
print (sort_cil8['car'].head(3))

#srednja potrosnja auta sa 6 cilindara
potrosnja_cil6=mtcars[mtcars.cyl==6]
avg_potrosnja_cil6=potrosnja_cil6['mpg'].mean()
print("Srednja potrosnja auta sa 6 cilindara: ")
print (avg_potrosnja_cil6)

# srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs
cil4_masa=mtcars[(mtcars.cyl == 4) & (mtcars.wt>=2) & (mtcars.wt<=2.2)]
avg_cil4_masa=cil4_masa["mpg"].mean()
print ("Srednja potrosnja auta sa 4 cilindara mase između 2000 i 2200 lbs: ")
print (avg_cil4_masa)

# Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka? 
manual_automatic=mtcars.groupby('am').car  
print ("Rucni mjenjac 0, Automatski mjenjac 1 :")
print (manual_automatic.count())

# Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
automatic_over100hp=mtcars[ (mtcars.am== 1) & (mtcars.hp>100)]
print ("Broj automobila s automatskim mjenjačem i snagom preko 100 hp:")
print (automatic_over100hp["car"].count()) 

# Kolika je masa svakog automobila u kilogramima?
lbs=0.4536
index=0
for m in mtcars['wt']:
    masa=(m*1000)*lbs
    print("%d. auto: %.2f kg" % (index,masa))
    index+=1
