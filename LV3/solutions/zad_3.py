# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 15:07:32 2021

@author: Karlo
"""

import urllib
import pandas as pd
import xml.etree.ElementTree as ET


url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.1.2017&vrijemeDo=01.01.2018'

fullFile = urllib.request.urlopen(url).read()
root = ET.fromstring(fullFile)

df = pd.DataFrame(columns=('Mjerenje', 'Vrijeme'))

i = 0
while True:
    try:
        clan = root.getchildren()[i].getchildren()
    except:
        break
    row = dict(zip(['Mjerenje', 'Vrijeme'], [clan[0].text, clan[2].text]))
    row_ser = pd.Series(row)
    row_ser.name = i
    df = df.append(row_ser)
    df.Mjerenje[i] = float(df.Mjerenje[i])
    i = i + 1

#1
df.Vrijeme = pd.to_datetime(df.Vrijeme, utc=True)
df.plot(y='Mjerenje', x='Vrijeme');
df['month'] = df['Vrijeme'].dt.month
df['dayOfweek'] = df['Vrijeme'].dt.dayofweek

#2
data_sort = df.sort_values("Mjerenje")
print('Tri dana s najvecim koncentracijama P10 cestica su: \n',data_sort[['Mjerenje','Vrijeme','dayOfweek','month']].tail(3))

#3
izostali_dani = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] - df.groupby("month").Mjerenje.count()
izostali_dani.plot.bar()

#4
compare = df[(df.month == 11) | (df.month == 7)]
compare.boxplot(column = ['Mjerenje'], by = 'month')

#5
df['Vikend'] = ((df.dayOfweek >= 5) & (df.dayOfweek <= 6))
df.boxplot(column = ['Mjerenje'], by='Vikend')

df['dayOfweek'] = ((df.dayOfweek >= 0) & (df.dayOfweek <= 4))
df.boxplot(column = ['Mjerenje'], by='dayOfweek')
