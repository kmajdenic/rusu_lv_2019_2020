Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.
Ispravci koda:
Linija 3: raw_input ispravljeno u input
Linija 5: argument fnamex ispravljen u fname
Linija 6: ukoliko želimo koristiti naredbu exit() potrebno je dodati tip iznimke SystemExit nakon except
Linija 7: dodane zagrade() oko željenog ispisa
Linija 20:dodane zagrade() oko željenig ispisa

Ovom vježbom smo ostvarili uvid u git te način na koji on funkcionira.
Rješavali smo problemske zadatke u pythonu koje smo potom putem gita uploadali
na udaljeni repozitorij koji prati svaku našu izmjenu. Izmjene smo popratili s 
prigodnim commit porukama u kojima se nudi objašnjenje što je napravljeno i slično.
