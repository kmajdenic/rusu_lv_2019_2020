# -*- coding: utf-8 -*-
#Zadatak2
while True:
    try:
        grade=float(input("Unesite ocjenu od 0.0 do 1.0: "))
        if grade<=1.0 and grade>=0.0:
            break
        else: print("Pogrešan unos!")
    except ValueError:
        print("Pogrešan unos!")

if grade >=0.9:
    print("Unesena ocjena odgovara kategoriji A")
elif grade >=0.8 and grade <0.9:
    print("Unesena ocjena odgovara kategoriji B")
elif grade >=0.7 and grade <0.8:
    print("Unesena ocjena odgovara kategoriji C")
elif grade >=0.6 and grade <0.7:
    print("Unesena ocjena odgovara kategoriji D")
elif grade <0.6:
    print("Unesena ocjena odgovara kategoriji F")
