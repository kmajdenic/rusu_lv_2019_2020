﻿
# Housing Values in Suburbs of Boston

The  **medv**  variable is the target variable.

### Data description

The Boston data frame has 506 rows and 14 columns.

This data frame contains the following columns:

**_crim_**  
per capita crime rate by town.

**_zn_**  
proportion of residential land zoned for lots over 25,000 sq.ft.

_**indus**_  
proportion of non-retail business acres per town.

_**chas**_  
Charles River dummy variable (= 1 if tract bounds river; 0 otherwise).

_**nox**_  
nitrogen oxides concentration (parts per 10 million).

_**rm**_  
average number of rooms per dwelling.

_**age**_  
proportion of owner-occupied units built prior to 1940.

_**dis**_  
weighted mean of distances to five Boston employment centres.

_**rad**_  
index of accessibility to radial highways.

_**tax**_  
full-value property-tax rate per  $10,000.

_**ptratio**_  
pupil-teacher ratio by town.

_**black**_  
1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town.

_**lstat**_  
lower status of the population (percent).

_**medv**_  
median value of owner-occupied homes in  $1000s.

### Source

Harrison, D. and Rubinfeld, D.L. (1978) Hedonic prices and the demand for clean air. J. Environ. Economics and Management 5, 81–102.

Belsley D.A., Kuh, E. and Welsch, R.E. (1980) Regression Diagnostics. Identifying Influential Data and Sources of Collinearity. New York: Wiley.

U laboratorijskoj vježbi 4, korsitili smo nadgledano učenje i upoznali se s lineranim modelima za rješavanje
regresijskih problema. Naše linearne modele proširivali smo s baznim funkcijama.
Pri radu smo se oslanjali na Scikit-learn biblioteku koja omogućava strojno učenje
temeljeno na NumPy,SciPy i matplotlib bibliotekama. Koristili smo linearne regresijske modele i ridge regresiju.