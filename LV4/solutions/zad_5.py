import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

def MSE(xnew,x,y,indeksi_train,indeksi_test):
    xtrain = xnew[indeksi_train]
    ytrain = y_measured[indeksi_train]
    
    xtest = xnew[indeksi_test]
    ytest = y_measured[indeksi_test]
    
    linearModel = lm.LinearRegression()
    linearModel.fit(xtrain,ytrain)
    
    ytest_p = linearModel.predict(xtest)
    ytrain_p=linearModel.predict(xtrain)
    MSEtest = mean_squared_error(ytest, ytest_p)
    MSEtrain = mean_squared_error(ytrain,ytrain_p)

    #pozadinska funkcija vs model
    plt.figure(2)
    plt.plot(x,y_true,label='f')
    plt.plot(x, linearModel.predict(xnew),'r-',label='model')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.plot(xtrain[:,1],ytrain,'ok',label='train')
    plt.legend(loc = 4)

    return MSEtest,MSEtrain
 
x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]


# make polynomial features
poly2= PolynomialFeatures(degree=2)
poly7= PolynomialFeatures(degree=7)
poly16 = PolynomialFeatures(degree=16)
xnew2 = poly2.fit_transform(x)
xnew7 = poly7.fit_transform(x)
xnew16 = poly16.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnew16))
indeksi_train = indeksi[0:int(np.floor(0.95 * len(xnew16)))]
indeksi_test = indeksi[int(np.floor(0.95 * len(xnew16))) +1:len(xnew16)]
MSEtest2, MSEtrain2 = MSE(xnew2, x, y_measured, indeksi_train, indeksi_test)
MSEtest7, MSEtrain7 = MSE(xnew7, x, y_measured, indeksi_train, indeksi_test)
MSEtest16, MSEtrain16 = MSE(xnew16, x, y_measured, indeksi_train, indeksi_test)

print("Degree = 2; MSEtest = " +  str(MSEtest2) + " MSEtrain = " + str(MSEtrain2) + "\nDegree = 7; MSEtest = " +  str(MSEtest7) + " MSEtrain = " + str(MSEtrain7)
      + "\nDegree = 16; MSEtest = " +  str(MSEtest16) + " MSEtrain = " + str(MSEtrain16))
