from sklearn.datasets import load_boston
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures



boston = load_boston()
X = boston.data
y = boston.target
print(boston.feature_names)