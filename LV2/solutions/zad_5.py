import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


auto = pd.read_csv ('../Resources/mtcars.csv')

plt.scatter(auto.mpg, auto.hp,marker='o',c=auto.wt)
plt.colorbar().set_label('Masa automobila (t)')
plt.xlabel('Potrošnja automobila')
plt.ylabel('Konjske snage automobila')
plt.show()

print('Minimalna potrošnja iznosi: ',auto.mpg.min())
print('Maksimalna potrošnja iznosi: ',auto.mpg.max())
print('Prosječna potrošnja automobila iznosi: ',auto.mpg.mean())

