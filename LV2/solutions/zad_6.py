import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('../Resources/tiger.png')
imgplot = plt.imshow(img)
imgBright=np.array(img)
imgBright=img*3
imgplot = plt.imshow(imgBright)