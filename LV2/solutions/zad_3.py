import numpy as np
import matplotlib.pyplot as plt

np.random.seed(26) 
generatorVisina=np.random.randint(2,size=20)
visinaM=[]
visinaZ=[]

for i in generatorVisina:
    if i==1:
        visinaM.append(np.random.normal(180,7))
    else:
        visinaZ.append(np.random.normal(167,7))

plt.hist([visinaM,visinaZ] ,color=['blue','red'])
plt.xlabel("Visina u cm")
plt.ylabel("Broj osoba")

prosjek_muski=np.average(visinaM)
prosjek_zene=np.average(visinaZ)
plt.axvline(prosjek_muski,color='blue')
plt.axvline(prosjek_zene,color='red')
