#Zadatak 2
import re
fhand = open('../Resources/mbox-short.txt') 
usernames1=[]
usernames2=[]
usernames3=[]
usernames4=[]
usernames5=[]
fullFile=fhand.read()

#Ako sadrže najmanje jedno slovo a
names1=re.findall(r'\S+[a]+\S+@\S+', fullFile)
for name in names1:
    username=name.split("@")[0]
    usernames1.append(username)
    
#Ako sadrže točno jedno slovo a
names2=re.findall(r'\S+[a]+\S+@\S+', fullFile)
for name in names2:
    username=name.split("@")[0]
    count = 0
    for i in username:
        if i == 'a':
            count = count + 1
    if count==1:
        usernames2.append(username)
        
#ne sadrži slovo a
names3=re.findall(r'[^a]@\S+', fullFile)
for name in names3:
    username=name.split("@")[0]
    usernames3.append(username)

#sadrži jedan ili više numeričkih znakova (0 – 9)
names4=re.findall(r'\S+[0-9]+\S+@\S+', fullFile)
for name in names4:
    username=name.split("@")[0]
    usernames4.append(username)
    
#sadrži samo mala slova (a-z)
names5=re.findall(r'\S+[a-z]+@\S+', fullFile)
for name in names5:
    username=name.split("@")[0]
    usernames5.append(username)
