from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data(path="mnist.npz")

x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255


x_train_s = x_train_s.reshape(60000, 28, 28, 1)
x_test_s = x_test_s.reshape(10000, 28, 28, 1)


y_train_s = keras.utils.to_categorical(y_train, 10)
y_test_s = keras.utils.to_categorical(y_test, 10)


model = keras.Sequential()
model.add(layers.Input(shape=(28,28,1), name="ulaz"))
model.add(layers.Conv2D(32, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Conv2D(64, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Flatten())
model.add(layers.Dense(200, activation="relu"))
model.add(layers.Dropout(rate = 0.5))
model.add(layers.Dense(50, activation="relu"))
model.add(layers.Dense(10, activation="softmax", name="izlaz"))

model.summary()


history = model.compile(loss="categorical_crossentropy",
                        optimizer="adam",
                        metrics=["accuracy",])



batch_size = 32
epochs = 5
history = model.fit(x_train_s, y_train_s, batch_size = batch_size, epochs = epochs, validation_split=0.1)


plt.figure(1, figsize=(14,5))
plt.subplot(1,2,1)
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='valid')
plt.xlabel('Epoch')
plt.ylabel('Cross-Entropy Loss')
plt.legend()

plt.subplot(1,2,2)
plt.plot(history.history['accuracy'], label='train')
plt.plot(history.history['val_accuracy'], label='valid')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend()
plt.show()


score = model.evaluate(x_test_s, y_test_s, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])

decision = model.predict(x_test_s, batch_size=batch_size)
y_pred = (decision > 0.5)
confusionMatrix = confusion_matrix(y_test_s.argmax(axis=1), y_pred.argmax(axis=1))
print(confusionMatrix)

model.save("CNN/")


