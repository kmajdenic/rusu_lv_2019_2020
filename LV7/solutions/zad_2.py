from keras.models import load_model
from matplotlib import pyplot as plt
from skimage.transform import resize
from skimage import color
import matplotlib.image as mpimg
import numpy as np
import cv2

filename = 'test2.png'

img = mpimg.imread(filename)
img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
img = resize(img,(28,28))


plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))
plt.show()


img = img.reshape(1,28,28,1)
img = img.astype('float32')

# TODO: ucitaj model
model = load_model("CNN/")

# TODO: napravi predikciju 
digit = np.argmax(model.predict(img), axis=-1)

# TODO: ispis rezultat
print("------------------------")
print(digit)

