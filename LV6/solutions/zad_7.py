# -*- coding: utf-8 -*-
import numpy as np
from sklearn import preprocessing
import sklearn.metrics as sm
from sklearn.metrics import classification_report
import sklearn.linear_model as lm
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def plot_decision_boundary(pred_func):
    # Set min and max values and give it some padding
    x_min, x_max = data_train[:, 0].min() - .5, data_train[:, 0].max() + .5
    y_min, y_max = data_train[:, 1].min() - .5, data_train[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Set3)
    plt.scatter(data_train[:, 0], data_train[:, 1], c=data_train[:,2], cmap=plt.cm.Set3)


def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

poly = PolynomialFeatures(degree=3, include_bias = False)
np.random.seed(242)
data_train=generate_data(200)
preprocessing.scale(data_train[:,:-1])

np.random.seed(12)
data_test=generate_data(100)
preprocessing.scale(data_train[:,:-1])

plt.figure()
plt.scatter(data_train[:,0],data_train[:,1])
plt.scatter(data_test[:,0], data_test[:,1], color='red')
plt.show()

data_train_new = poly.fit_transform(data_train[:,0:2])

log_model = lm.LogisticRegression()
log_model.fit(data_train[:,:-1],data_train[:,2])
test_log_model=log_model.predict(data_test[:,:-1])

plt.figure()
for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    plt.scatter(data_train[i,0],data_train[i,1],c=color, zorder=2) 
plot_decision_boundary(lambda x: log_model.predict(x))
plt.title("Granica odluke logisticke regresije")
plt.show()


f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,
                          min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = log_model.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)

ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()

plt.figure()
for i in range (0,len(data_test)):
    if(data_test[i,2]) == test_log_model[i]:
        color="green"  #ispravno klasificirani podaci
    else:
        color="black"  #pogresno klasificirani podaci
    plt.scatter(data_test[i,0],data_test[i,1],c=color,)
plt.title("Logistic Regression")
plt.title('Klasifikacija testnog skupa logistickom regresijom')
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

confusion_mat=sm.confusion_matrix(data_test[:,2],test_log_model[:])

plot_confusion_matrix(confusion_mat)

print("Vrjednovanje klasifikatora %s:\n%s\n"% (log_model, classification_report(data_test[:,2], test_log_model[:])))


