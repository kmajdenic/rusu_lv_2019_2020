# -*- coding: utf-8 -*-
import numpy as np
from sklearn import neural_network
from sklearn import preprocessing
import sklearn.metrics as sm
from sklearn.metrics import classification_report
import sklearn.linear_model as lm
import matplotlib.pyplot as plt

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def plot_decision_boundary(pred_func):
    # Set min and max values and give it some padding
    x_min, x_max = data_train[:, 0].min() - .5, data_train[:, 0].max() + .5
    y_min, y_max = data_train[:, 1].min() - .5, data_train[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Set3)
    plt.scatter(data_train[:, 0], data_train[:, 1], c=data_train[:,2], cmap=plt.cm.Set3)



np.random.seed(242)
data_train=generate_data(200)
preprocessing.scale(data_train[:,:-1])

np.random.seed(12)
data_test=generate_data(100)
preprocessing.scale(data_train[:,:-1])

plt.figure()
plt.scatter(data_train[:,0],data_train[:,1])
plt.scatter(data_test[:,0], data_test[:,1], color='red')
plt.show()

log_model = lm.LogisticRegression()
log_model.fit(data_train[:,:-1],data_train[:,2])
test_log_model=log_model.predict(data_test[:,:-1])











