# -*- coding: utf-8 -*-
"""
Created on Sat Jan 29 17:16:14 2022

@author: Karlo
"""
import pandas as pd
from shutil import copy2
import os


test_data = pd.read_csv('Test.csv')
print(test_data)
path = os.getcwd()
os.makedirs('Test_Dir', exist_ok=True)
for i in range (43):
    os.makedirs(path + '/Test_Dir/' + str(i))
    
    
for  _, row in test_data.iterrows():
    endpath = row['Path'].split('/')
    name = endpath[1]
    copy2(row.Path, path + '/Test_Dir' + str(row.ClassId) + '/' + name)

