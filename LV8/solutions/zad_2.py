# -*- coding: utf-8 -*-
from tensorflow import keras
from keras import layers

    
train_ds = keras.preprocessing.image_dataset_from_directory(directory='Train/', labels='inferred', label_mode='categorical', batch_size=32, image_size=(48, 48))

model = keras.Sequential()
model.add(keras.Input(shape=(48,48,3)))
model.add(layers.Conv2D(32, kernel_size=(3,3),padding='same', activation="relu"))
model.add(layers.Conv2D(32, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(64, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(64, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(128, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(128, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Flatten())
model.add(layers.Dense(units=512, activation='relu'))
model.add(layers.Dropout(rate=0.5))
model.add(layers.Dense(units=43, activation='softmax'))
model.summary()
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit_generator(train_ds, epochs=5)

    
test_ds = keras.image_dataset_from_directory(directory='Test_Dir/', labels='inferred', label_mode='categorical', batch_size=32, image_size=(48, 48))
predict = model.evaluate_generator(test_ds)
print(predict)
