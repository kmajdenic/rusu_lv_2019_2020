from sklearn import datasets
import numpy as np
import sklearn.cluster as cluster
import matplotlib.pyplot as pltpy

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X


X = generate_data(500, 5)

kMeans = cluster.KMeans(n_clusters = 5)

predict = kMeans.fit_predict(X)


for i in range(5):
    xColor = []
    yColor = []
    for j in range(len(predict)):
        xColor.append(X[j,0])
        yColor.append(X[j,1])
        
    pltpy.scatter(xColor, yColor)


listRange = list(range(1,20))
critFunction = []
for N_clusters in listRange:
    clusterK = cluster.KMeans(n_clusters = N_clusters)
    predicted = clusterK.fit_predict(X)
    critFunction.append(clusterK.inertia_)
    
        
pltpy.bar(x = listRange, height = critFunction, color = "blue")
pltpy.plot(listRange,critFunction, c = "red")
pltpy.xlabel("Cluster number")
pltpy.ylabel("Criterial function")
pltpy.xticks(listRange)
pltpy.show()
