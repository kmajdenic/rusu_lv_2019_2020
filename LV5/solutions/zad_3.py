from sklearn import datasets
import numpy as np
import sklearn.cluster as cluster
import matplotlib.pyplot as pltpy
from scipy.cluster.hierarchy import dendrogram, linkage


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X


X = generate_data(500, 5)

linkedCentroid = linkage(X, 'centroid')
pltpy.figure()
dend = dendrogram(linkedCentroid)

linkedMedian = linkage(X, 'median')
pltpy.figure()
dend = dendrogram(linkedMedian)

linkedAverage = linkage(X, 'average')
pltpy.figure()
dend = dendrogram(linkedAverage)

linkedWard = linkage(X, 'ward')
pltpy.figure()
dend = dendrogram(linkedWard)

linkedSingle = linkage(X, 'single')
pltpy.figure()
dend = dendrogram(linkedSingle)

linkedComplete = linkage(X, 'complete')
pltpy.figure()
dend = dendrogram(linkedComplete)

pltpy.show()

